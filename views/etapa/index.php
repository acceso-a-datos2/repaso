<?php

use app\models\Etapa;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Etapas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="etapa-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Etapa', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'numetapa',
            'kms',
            'salida',
            'llegada',
            'dorsal',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Etapa $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'numetapa' => $model->numetapa]);
                 }
            ],
        ],
    ]); ?>


</div>
