<?php
use yii\helpers\Html;
/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
    <div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">
        
         <div class="row">

            <div class="col-sm-6 col-md-4">
                <div class="card alturaminina">
                    <div class="card-body tarjeta">
                        <h4> Consulta 1 </h4>
                        <p> Listar las edades de los ciclistas sin repetidos </p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta1'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta1'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>
                </div>
            </div>
              <div class="col-sm-6 col-md-4">
                <div class="card alturaminina">
                    <div class="card-body tarjeta">
                        <h4> Consulta 1 </h4>
                        <p> Listar las edades de los ciclistas sin repetidos </p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta1'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta1'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>
                </div>
            </div>
                 <div class="col-sm-6 col-md-4">
                <div class="card alturaminina">
                    <div class="card-body tarjeta">
                        <h4> Consulta 1 </h4>
                        <p> Listar las edades de los ciclistas sin repetidos </p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta1'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta1'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>
                </div>
            </div>


             
            </div>
    </div>

    </div>
</div>
